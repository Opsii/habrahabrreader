//
//  HHPostFocusViewController.h
//  HabrahabrReader
//
//  Created by Vinnichenko Dmitry on 6/26/15.
//  Copyright (c) 2015 Vinnichenko Dmitry. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface HHPostFocusViewController : UIViewController

@property (strong, nonatomic) HHPost* post;

@end
