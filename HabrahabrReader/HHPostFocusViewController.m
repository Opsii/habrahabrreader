//
//  HHPostFocusViewController.m
//  HabrahabrReader
//
//  Created by Vinnichenko Dmitry on 6/26/15.
//  Copyright (c) 2015 Vinnichenko Dmitry. All rights reserved.
//

#import "HHPostFocusViewController.h"

@interface HHPostFocusViewController ()

@property (weak, nonatomic) IBOutlet UIWebView *webView;

@end

@implementation HHPostFocusViewController

#pragma mark - Lifecycle -

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    [SVProgressHUD showWithStatus:@"Loading" maskType:SVProgressHUDMaskTypeGradient];
    [[HHNetworkManager defaultManager] checkConnectionWithCompletition:^(NSError *error) {
        [SVProgressHUD dismiss];
        if (!error)
            [self p_setupWebView];
        else
            [SVProgressHUD showErrorWithStatus:error.localizedDescription];
    }];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Private -

- (void)p_setupWebView {
    NSURL* postURL = [NSURL URLWithString: [self.post.link stringByAddingPercentEscapesUsingEncoding: NSUTF8StringEncoding]];
    NSURLRequest* request = [NSURLRequest requestWithURL:postURL];
    [self.webView loadRequest:request];
}

@end
