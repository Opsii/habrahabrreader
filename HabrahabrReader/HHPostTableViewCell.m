//
//  HHPostTableViewCell.m
//  HabrahabrReader
//
//  Created by Vinnichenko Dmitry on 6/26/15.
//  Copyright (c) 2015 Vinnichenko Dmitry. All rights reserved.
//

#import "HHPostTableViewCell.h"


static NSString* HHDateFormat = @"EEE, dd MM yyyy HH:mm:ss z";

static NSUInteger HHTitleLeftOffet = 13;
static NSUInteger HHTitleRightOffet = 13;


@implementation HHPostTableViewCell

#pragma mark - Default -

- (void)awakeFromNib {
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

- (void)layoutSubviews {
    
    /* Dynamic height */
    if(!IS_IOS8)
    {
        [super layoutSubviews];
        [self.title setPreferredMaxLayoutWidth:([UIScreen mainScreen].bounds.size.width - HHTitleLeftOffet - HHTitleRightOffet)];
    }
}

#pragma mark - Public -

- (void)setupPostWith:(HHPost*)post {
    self.title.text = post.title;
    self.date.text = [self p_getFormattedDate:post.date];
}

#pragma mark - Private -

- (NSString*)p_getFormattedDate:(NSString*)date {
    
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    [dateFormatter setTimeZone:[NSTimeZone localTimeZone]];
    [dateFormatter setDateFormat:HHDateFormat];
    
    NSDate *fullDate = [dateFormatter dateFromString:date];
    NSDate *currentDate = [NSDate date];
    
    if (fullDate != nil) {
        NSDateComponents *componentsFullDate = [[NSCalendar currentCalendar] components:
                                              NSCalendarUnitDay |
                                              NSCalendarUnitMonth |
                                              NSCalendarUnitYear |
                                              NSCalendarUnitHour |
                                              NSCalendarUnitMinute
                                                                             fromDate:fullDate];
        
        NSDateComponents *componentsCurrent = [[NSCalendar currentCalendar] components:
                                               NSCalendarUnitDay |
                                               NSCalendarUnitMonth |
                                               NSCalendarUnitYear |
                                               NSCalendarUnitHour |
                                               NSCalendarUnitMinute
                                                                              fromDate:currentDate];
        
        return [self p_stringFormatter:[self p_formatDiferenceDay:[componentsCurrent day] - [componentsFullDate day]]
                                 month:[componentsFullDate month]
                                   day:[componentsFullDate day]
                             countHour:[componentsFullDate hour]
                            countMinut:[componentsFullDate minute]];
        }
    return date;
}

- (NSString*)p_stringFormatter:(NSString*)string month:(NSInteger)month day:(NSInteger)day countHour:(NSInteger)hour countMinut:(NSInteger)min {
    NSString* minute = min < 10? [NSString stringWithFormat:@"0%li", (long)min] : [NSString stringWithFormat:@"%li", (long)min];
    
    if (!string.length)
        return [NSString stringWithFormat:@"%li.%li в %li:%@", (long)month, (long)day, (long)hour, minute];
    else
        return [NSString stringWithFormat:@"%@ в %li:%@", string, (long)hour, minute];
}

- (NSString *)p_formatDiferenceDay:(NSInteger)countDayBetween {
    if (countDayBetween == 0)
        return @"сегодня";
    else if (countDayBetween == 1)
        return @"вчера";
    else if (countDayBetween < 5)
        return @"недавно";
    else if (countDayBetween < 10)
        return @"давно";
    else
        return @"очень давно";
}

@end
