//
//  HHNewsViewController.m
//  HabrahabrReader
//
//  Created by Vinnichenko Dmitry on 6/25/15.
//  Copyright (c) 2015 Vinnichenko Dmitry. All rights reserved.
//

#import "HHNewsViewController.h"

static NSString* const kPostCellIdentifier = @"PostCell";

static NSUInteger const HHDefaultPostCellHeight = 62;

@interface HHNewsViewController () <UITableViewDataSource, UITableViewDelegate>

@property (weak, nonatomic) IBOutlet UITableView *tableView;
@property (strong, nonatomic) HHPostTableViewCell* postCell; // For find dynamic cell height

@property (strong, nonatomic) NSArray* content;

- (IBAction)updateAction:(UIBarButtonItem *)sender;

@end

@implementation HHNewsViewController

#pragma mark - Lifecycle -

- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.tableView.rowHeight = UITableViewAutomaticDimension;
    self.tableView.estimatedRowHeight = HHDefaultPostCellHeight;
    
    [self p_updateContent];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    if ([segue.identifier isEqualToString:@"post_link"] && [sender isKindOfClass:[UITableViewCell class]]) {
        NSIndexPath* objectIndex = [self.tableView indexPathForCell:(UITableViewCell*)sender];
        HHPost* post = [_content objectAtIndex:objectIndex.row];
        [segue.destinationViewController setPost:post];
    }
}

#pragma mark - IBActions -

- (IBAction)updateAction:(UIBarButtonItem *)sender {
    [self p_updateContent];
}

#pragma mark - Private -

- (void)p_updateContent {
    [SVProgressHUD showWithStatus:@"Loading" maskType:SVProgressHUDMaskTypeGradient];
    [[HHNetworkManager defaultManager]getLastNewsWithCompletition:^(NSArray *result, NSError *error) {
        [SVProgressHUD dismiss];
        if (!error) {
            _content = result;
        }
        else {
            [SVProgressHUD showErrorWithStatus:error.localizedDescription];
            _content = [[HHDataManager defaultManager] getDBNews];
        }
        [_tableView reloadSections:[NSIndexSet indexSetWithIndex:0] withRowAnimation:UITableViewRowAnimationFade];
    }];
}

- (CGFloat)p_resizeHeightCellAtIndexPath:(NSIndexPath*)indexPath {
    if (IS_IOS8)
        return UITableViewAutomaticDimension;
    
    HHPost* post = [_content objectAtIndex:indexPath.row];
    
    [self.postCell setupPostWith:post];
    
    self.postCell.bounds = CGRectMake(0.0f, 0.0f, CGRectGetWidth(self.tableView.bounds), CGRectGetHeight(self.tableView.bounds));
    [self.postCell setNeedsLayout];
    [self.postCell layoutIfNeeded];
    CGFloat height = [self.postCell.contentView systemLayoutSizeFittingSize:UILayoutFittingCompressedSize].height;
    return height;
}

#pragma mark - Getters -

- (HHPostTableViewCell*)postCell {
    if (!_postCell) {
        _postCell = [self.tableView dequeueReusableCellWithIdentifier:kPostCellIdentifier];
    }
    return _postCell;
}

#pragma mark - <UITableViewDataSource> -

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    /* Find dynamic cell height */
    return [self p_resizeHeightCellAtIndexPath:indexPath];
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return [_content count];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    HHPost* post = [self.content objectAtIndex:indexPath.row];
    
    HHPostTableViewCell* cell = [tableView dequeueReusableCellWithIdentifier:kPostCellIdentifier forIndexPath:indexPath];
    [cell setupPostWith:post];
    
    return cell;
}

@end
