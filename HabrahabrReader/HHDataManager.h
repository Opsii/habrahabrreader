//
//  HHDataManager.h
//  HabrahabrReader
//
//  Created by Vinnichenko Dmitry on 6/25/15.
//  Copyright (c) 2015 Vinnichenko Dmitry. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface HHDataManager : NSObject

+ (HHDataManager*)defaultManager;

- (NSArray*)getDBNews;
- (void)deleteDBNews;
- (void)saveDBNews;

@end
